#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://github.com/Palanthis 
# License	:	Distributed under the terms of GNU GPL v3
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################

sudo apt install -y --no-install-recommends xserver-xorg-core xserver-xorg xfonts-base xinit mesa-va-drivers
sudo apt install -y --no-install-recommends xserver-common xserver-xorg-input-all xserver-xorg-input-libinput
sudo apt install -y --no-install-recommends xserver-xorg-video-fbdev xserver-xorg-video-fbturbo mesa-vdpau-drivers
sudo apt install -y --no-install-recommends xfce4 desktop-base lightdm xfce4-terminal audacious ristretto
sudo apt install -y --no-install-recommends mousepad file-roller thunar-volman
sudo apt install -y --no-install-recommends xfwm4 xfce4-panel xfce4-settings xfce4-session xfce4-terminal
sudo apt install -y --no-install-recommends xfdesktop4 tango-icon-theme xfce4-utils xfce4-taskmanager






